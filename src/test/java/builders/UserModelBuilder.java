package builders;

import models.UserModel;

public final class UserModelBuilder {
    private String name;
    private String job;

    private UserModelBuilder() {
    }

    public static UserModelBuilder aUserModel() {
        return new UserModelBuilder();
    }

    public UserModelBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserModelBuilder withJob(String job) {
        this.job = job;
        return this;
    }

    public UserModel build() {
        UserModel userModel = new UserModel();
        userModel.setName(name);
        userModel.setJob(job);
        return userModel;
    }
}
