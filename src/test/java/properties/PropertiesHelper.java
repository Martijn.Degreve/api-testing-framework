package properties;

public class PropertiesHelper {

    private static final PropertiesHelper instance = new PropertiesHelper();
    private String url;

    public static PropertiesHelper getInstance() {
        return instance;
    }

    private PropertiesHelper() {
        url = System.getProperty("url");
    }

    public String getUrl(){
        if (url == null){
            return ConfigFileReader.getInstance().getUrl();
        } else{
            return url;
        }
    }
}
