package properties;

import java.io.*;
import java.util.Properties;

class ConfigFileReader {

    private static final ConfigFileReader instance = new ConfigFileReader();
    private static final String CONFIGURATION_PROPERTIES = "configuration.properties";

    private Properties properties;


    static ConfigFileReader getInstance(){
        return instance;
    }

    private ConfigFileReader() {
        loadProperties();
    }

    private void loadProperties() {
        try {
            properties = new Properties();
            properties.load(getPropertiesFile());
            getPropertiesFile().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private InputStream getPropertiesFile() {
        return getClass().getClassLoader().getResourceAsStream(CONFIGURATION_PROPERTIES);
    }

    String getUrl(){
        String url = properties.getProperty("url");
        if(url != null) return url;
        else throw new RuntimeException("Url not specified in the Configuration.properties file for the Key:url");
    }
}
