package helpers;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import models.UserModel;
import parsers.UserPostBodyParser;


public class HttpHelper {

    public HttpResponse<JsonNode> performGetRequest(String getUrl) throws UnirestException {
        return Unirest.get(getUrl)
                .header("Content-Type", "application/json")
                .asJson();
    }

    public HttpResponse<JsonNode> performPostRequest(String usersUrl, UserModel user) throws UnirestException {
        UserPostBodyParser userPostBodyParser = new UserPostBodyParser();
        String userBody = userPostBodyParser.parseBody(user);
        return Unirest.post(usersUrl)
                .body(userBody)
                .asJson();
    }
}