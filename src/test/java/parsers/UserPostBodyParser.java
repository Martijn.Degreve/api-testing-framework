package parsers;

import models.UserModel;

public class UserPostBodyParser {
    public String parseBody(UserModel user) {
        return "{\n" +
                "    \"name\": \"" + user.getName() + "\",\n" +
                "    \"job\": \"" + user.getJob() + "\",\n" +
                "}";
    }
}