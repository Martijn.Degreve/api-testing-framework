package tests;

import builders.UserModelBuilder;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import helpers.HttpHelper;
import models.UserModel;
import org.junit.Test;
import properties.PropertiesHelper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ExampleTest {

    private String baseUrl = PropertiesHelper.getInstance().getUrl();
    private String usersUrl = baseUrl + "/api/users";

    private HttpHelper httpHelper = new HttpHelper();

    @Test
    public void singleUserGetTest() throws UnirestException {
        HttpResponse<JsonNode> getResponse = httpHelper.performGetRequest(usersUrl + "/2");
        assertEquals(200, getResponse.getStatus());
        assertEquals("Janet", getValueFromResponse(getResponse,"first_name"));
    }

    @Test
    public void postUserTest() throws UnirestException {
        UserModel user = UserModelBuilder.aUserModel()
                .withName("Tommy")
                .withJob("Researcher").build();

        HttpResponse<JsonNode> postResponse = httpHelper.performPostRequest(usersUrl, user);
        assertEquals(201, postResponse.getStatus());
    }

    private String getValueFromResponse(HttpResponse<JsonNode> getResponse,String key) {
        return getResponse.getBody().getArray().getJSONObject(0).getJSONObject("data").getString(key);
    }
}